# pirati.cz program point popularity measurer

A quick backend server to sort the 2022 communal election points' popularity on pirati.cz, not yet integrated into the newer website system.

## Setup

- Copy `config.example.json` to `config.json`.
- Change the `SECRET_KEY` and `IDENTIFIER_HASH_PEPPER` to random values with very high entropy.
- Set the `DATABASE_URL` environment variable to something like `postgresql://username:password@hostname/dbname`.
- Run the app with your WSGI server of choice that integrates the `create_app()` function. Personally, I like gunicorn. (`gunicorn "measurer:create_app()"`)
