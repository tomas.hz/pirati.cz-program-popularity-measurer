# syntax=docker/dockerfile:3

# https://sourcery.ai/blog/python-docker/
# Thanks to Brendan Maginnis!

FROM python:3.10 as base

# Setup env
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONFAULTHANDLER 1
ENV DEBIAN_FRONTEND noninteractive

FROM base AS python-deps

# Install pipenv and compilation dependencies
RUN pip install -U virtualenv pipenv
RUN apt-get update
RUN apt-get install -yq --no-install-recommends gcc

# Install python dependencies in /.venv
COPY Pipfile .
COPY Pipfile.lock .
RUN PIPENV_VENV_IN_PROJECT=1 pipenv install --deploy
RUN PIPENV_VENV_IN_PROJECT=1 pipenv install gunicorn

FROM base AS runtime

# Install Postgres
RUN apt-get update
RUN apt-get install -yq --no-install-recommends postgresql

# Copy virtual env from python-deps stage
COPY --from=python-deps /.venv /.venv
ENV PATH="/.venv/bin:$PATH"

# Create and switch to a new user
RUN useradd --create-home appuser
WORKDIR /home/appuser
USER appuser

# Install application into container
COPY . .

# Expose API port
EXPOSE 5009
