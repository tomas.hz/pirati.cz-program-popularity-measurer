"""Utilities."""

import base64

import argon2
import flask

__all__ = ["get_ip_hash"]

def get_ip_hash() -> str:
	r"""Generates a base64-encoded Argon2id hash for the current Flask request's
	remote address, with the current app's ``IDENTIFIER_HASH_PEPPER`` config key
	as the salt (pepper).

	Since hashes will always be Argon2id and share the same parameters, to save
	storage, extra data usually present is not included and only the raw hash is
	returned.

	:returns: The hash.

	.. note::
		A shared pepper isn't the best from a security standpoint, but the best we
		can afford to use here, since this function is primarily used to find
		existing rows based on the hash's result - for example, guests with the
		same IP address. For this to happen at a reasonable speed, salt values
		random for each user cannot be used.

	.. note::
		To avoid leaking the secret key, the pepper is separate.

	.. seealso::
		`Wikipedia - Pepper (cryptography), on Wikiless <https://wikiless.org/\
		wiki/Pepper_(cryptography)>`_
	"""

	return base64.b64encode(
		argon2.low_level.hash_secret_raw(
			# Support reverse proxy
			flask.request.headers.get("X-Forwarded-For", flask.request.remote_addr),
			
			salt=flask.current_app.config["IDENTIFIER_HASH_PEPPER"].encode("utf-8"),
			time_cost=flask.current_app.config["IDENTIFIER_HASH_TIME_COST"],
			memory_cost=flask.current_app.config["IDENTIFIER_HASH_MEMORY_COST"],
			parallelism=flask.current_app.config["IDENTIFIER_HASH_PARALLELISM"],
			hash_len=32,
			type=argon2.low_level.Type.ID
		)
	).decode("utf-8") 
