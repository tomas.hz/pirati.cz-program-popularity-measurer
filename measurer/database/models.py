import datetime

import sqlalchemy

from . import Base
from .utils import UUID, get_uuid

from .. import utils

__all__ = ["Vote"]


class Vote(Base):
	"""A positive vote. We don't have negative ones."""

	__tablename__ = "votes"

	id = sqlalchemy.Column(
		UUID,
		primary_key=True,
		default=get_uuid
	)

	point = sqlalchemy.Column(
		sqlalchemy.String(128),
		nullable=False
	)
	"""The program point the vote was made on."""

	identifier = sqlalchemy.Column(
		sqlalchemy.String(64),
		default=utils.get_ip_hash,
		nullable=False
	)
	"""An identifier of the person who made the vote. This should be, for example,
	a securely hashed IP address. As securely as IP addresses can be hashed, that
	is.
	"""
