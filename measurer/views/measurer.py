import http.client
import typing

import flask
import sqlalchemy
import werkzeug.exceptions

from .. import database, utils, validators


measurer_blueprint = flask.Blueprint(
	"measurer",
	__name__
)


@measurer_blueprint.route("/", methods=["POST"])
@validators.validate_json({
	"point": {
		"type": "string",
		"allowed": flask.current_app.config["POINTS"],
		"nullable": False
	}
})
def add_vote() -> typing.Union[flask.Response, int]:
	vote_exists = flask.g.sa_session.execute(
		sqlalchemy.select(database.Vote).
		where(
			sqlalchemy.and_(
				database.Vote.point == flask.g.json["point"],
				database.Vote.identifier == utils.get_ip_hash()
			)
		).
		exists().
		select()
	).scalars().one()

	if vote_exists:
		raise werkzeug.exceptions.Forbidden

	new_vote = database.Vote(point=flask.g.json["point"])
	flask.g.sa_session.add(new_vote)
	flask.g.sa_session.commit()

	return flask.jsonify(None), http.client.NO_CONTENT


@measurer_blueprint.route("/", methods=["DELETE"])
@validators.validate_json({
	"point": {
		"type": "string",
		"allowed": flask.current_app.config["POINTS"],
		"nullable": False
	}
})
def delete_vote() -> typing.Union[flask.Response, int]:
	vote = flask.g.sa_session.execute(
		sqlalchemy.select(database.Vote).
		where(
			sqlalchemy.and_(
				database.Vote.point == flask.g.json["point"],
				database.Vote.identifier == utils.get_ip_hash()
			)
		)
	).scalars().one_or_none()

	if vote is None:
		raise werkzeug.exceptions.NotFound

	flask.g.sa_session.delete(vote)
	flask.g.sa_session.commit()

	return flask.jsonify(None), http.client.NO_CONTENT


@measurer_blueprint.route("/", methods=["GET"])
def list_votes() -> typing.Union[flask.Response, int]:
	votes = flask.g.sa_session.execute(
		sqlalchemy.select(
			database.Vote.point,
			sqlalchemy.func.count(database.Vote.point)
		).
		group_by(database.Vote.point)
	).all()

	result = {}

	for vote in votes:
		result[vote[0]] = vote[1]

	return flask.jsonify(result), http.client.OK
