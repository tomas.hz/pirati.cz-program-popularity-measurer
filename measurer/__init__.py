import json
import os
import typing

import flask
import sqlalchemy.orm

__all__ = ["create_app"]


def create_app() -> flask.Flask:
	"""Creates a pre-configured :class:`Flask <flask.Flask>` app.

	If the ``CONFIG_LOCATION`` environment variable is set, loads the JSON config
	file located where it describes. Otherwise, attempts to load the current
	working directory's ``config.json`` as a fallback.

	:returns: The app.
	"""

	app = flask.Flask(__name__)

	with app.app_context():
		app.logger.info("Setting up app")

		app.logger.debug("Loading config file")

		app.config.from_file(
			os.environ.get(
				"CONFIG_LOCATION",
				os.path.join(
					os.getcwd(),
					"config.json"
				)
			),
			load=json.load
		)

		app.logger.debug("Creating engine")

		sa_engine = sqlalchemy.create_engine(os.environ["DATABASE_URL"])

		app.sa_session_class = sqlalchemy.orm.scoped_session(
			sqlalchemy.orm.sessionmaker(
				bind=sa_engine
			)
		)

		app.logger.debug("Setting up extensions")

		@app.before_request
		def before_request() -> None:
			"""TODO: doc"""

			flask.g.sa_session = flask.current_app.sa_session_class()

		@app.teardown_request
		def teardown_request(
			exception: typing.Union[None, Exception]
		) -> None:
			"""Attempts to commit :attr:`flask.g.sa_session` and rolls it back if
			any exception is raised during the process. The exception is then
			logged.

			:param exception: The exception that occurred in the prior request,
				if there was any.
			"""

			if "sa_session" in flask.g:
				# "Clean" the session
				try:
					flask.g.sa_session.commit()
				except Exception as commit_exception:
					flask.g.sa_session.rollback()

					flask.current_app.logger.error(
						"Exception %s raised during the request teardown session commit: %s",
						commit_exception.__class__.__name__,
						(
							commit_exception
							if hasattr(commit_exception, "__str__")
							else "no details"
						)
					)

				flask.current_app.sa_session_class.remove()

		from .views import measurer_blueprint

		for blueprint in (measurer_blueprint,):
			app.logger.debug(
				"Registering blueprint: %s",
				blueprint
			)

			app.register_blueprint(blueprint)

		@app.cli.command("reflect")
		def reflect() -> None:
			"""Reflects all models to the app's database."""

			from .database import Base

			Base.metadata.create_all(bind=sa_engine)

		return app
