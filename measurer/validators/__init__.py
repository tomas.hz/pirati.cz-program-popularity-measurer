"""Validation using the Cerberus library. To extend, see its documentation
`here <https://docs.python-cerberus.org/en/stable/index.html>`_.
"""

from __future__ import annotations

import base64
import datetime
import functools
import re
import typing
import uuid

import cerberus
import flask
import werkzeug.exceptions

import validators

from .. import types

__all__ = ["APIValidator", "validate_json"]
__version__ = "1.12.0"


class APIValidator(cerberus.Validator):
	"""Cerberus validator with extended functionality."""

	def _check_with_is_base64(
		self: APIValidator,
		field: str,
		value: typing.Union[None, str]
	) -> None:
		"""Checks whether or not ``value`` is a valid base64 string, without
		coercing it into the bytes it represents. This is useful where external
		tools require untouched base64 strings.

		:param field: The current field.
		:param value: The current field's value. If :data:`None`, this means the
			field is nullable. Nothing happens in this case.
		"""

		if value is None:
			return

		try:
			base64.b64decode(value, validate=True)
		except ValueError:
			self._error(
				field,
				"must be a valid base64 string"
			)

	def _check_with_is_email(
		self: APIValidator,
		field: str,
		value: typing.Union[None, str]
	) -> None:
		"""Checks whether or not ``value`` is a valid email address.

		:param field: The current field.
		:param value: The current field's value. If :data:`None`, this means the
			field is nullable. Nothing happens in this case.
		"""

		if value is None:
			return

		if not validators.email(value):
			self._error(
				field,
				"must be a valid email address"
			)

	def _check_with_is_valid_regex(
		self: APIValidator,
		field: str,
		value: typing.Union[None, str]
	) -> None:
		"""Checks whether or not ``value`` is a valid regular expression.

		:param field: The current field.
		:param value: The current field's value. If :data:`None`, this means the
			field is nullable. Nothing happens in this case.
		"""

		if value is None:
			return

		try:
			re.compile(value)
		except re.error:
			self._error(field, "must be a valid regular expression")

	def _check_with_is_public_url(
		self: APIValidator,
		field: str,
		value: typing.Union[None, str]
	) -> None:
		"""Checks that ``value`` is a valid URL. If
		:attr:`debug <heiwa.ConfiguredLockFlask.debug>` mode is not enabled, the
		URL must also correspond to a public resource.

		:param field: The current field.
		:param value: The current field's value. If :data:`None`, this means the
			field is nullable. Nothing happens in this case.
		"""

		if value is None:
			return

		if not validators.url(
			value,
			public=(not flask.current_app.debug)
		):
			self._error(
				field,
				"must be a valid public URL"
			)

	def _check_with_has_no_duplicates(
		self: APIValidator,
		field: str,
		value: typing.Union[None, typing.List[typing.Any]]
	) -> None:
		"""Checks that the list in ``value`` contains no duplicate items.

		:param field: The current field.
		:param value: The current field's value. If :data:`None`, this means the
			field is nullable. Nothing happens in this case.
		"""

		if value is None:
			return

		if len(value) != len(set(value)):
			self._error(
				field,
				"must contain no duplicate items"
			)

	def _normalize_coerce_convert_to_uuid(
		self: APIValidator,
		value: typing.Union[None, str]
	) -> typing.Union[None, uuid.UUID]:
		"""Converts the ``value`` to an :class:`UUID <uuid.UUID>`.

		:param value: The current field's value. If :data:`None`, this means the
			field is nullable. Nothing happens in this case.

		:returns: The converted UUID.
		"""

		if value is None:
			return None

		return uuid.UUID(value)

	def _normalize_coerce_convert_to_datetime(
		self: APIValidator,
		value: typing.Union[None, str]
	) -> typing.Union[None, datetime.datetime]:
		"""As long as ``value`` is a string formatted as per
		`ISO-8601 <https://wikiless.org/wiki/ISO_8601>`_, it's converted to a
		:class:`datetime <datetime.datetime>` object.

		:raises ValueError: Raised when the datetime does not have a specified
			time zone.

		:param value: The current field's value. If :data:`None`, this means the
			field is nullable. Nothing happens in this case.

		:returns: The converted date and time.
		"""

		if value is None:
			return None

		result = datetime.datetime.fromisoformat(value)

		if result.tzinfo is None:
			raise ValueError("must have a timezone")

		return result

	def _normalize_coerce_decode_base64(
		self: APIValidator,
		value: typing.Union[None, str]
	) -> typing.Union[None, bytes]:
		"""Converts the base64-encoded ``value`` to the bytes it represents.

		:param value: The current field's value. If :data:`None`, this means the
			field is nullable. Nothing happens in this case.

		:returns: The decoded bytes.
		"""

		if value is None:
			return None

		return base64.b64decode(
			value,
			validate=True
		)

	def _validate_length_divisible_by(
		self: APIValidator,
		divider: int,
		field: str,
		value: typing.Union[None, types.SupportsLength]
	) -> None:
		"""Checks whether or not the length of ``value`` is divisible by ``divider``.
		If not, an error is raised.

		:param divider: The required divider.
		:param field: The current field.
		:param value: The current field's value, which must support the ``__len__``
			method. If :data:`None`, this means the field is nullable. Nothing
			happens in this case.

		The rule's arguments are validated against this schema:
		{
			'type': 'integer'
		}
		"""

		if value is None:
			return

		if len(value) % divider != 0:
			self._error(
				field,
				f"length must be divisible by {divider}"
			)

	def _validate_not_null_dependencies(
		self: APIValidator,
		dependencies: typing.Union[
			typing.Iterable[str],
			str
		],
		field: str,
		value: typing.Any
	) -> None:
		"""The same as the ``dependencies`` rule (see `here https://docs.pytho`
		n-cerberus.org/en/stable/validation-rules.html#dependencies`_), but the
		specified elements must not be :data:`None`, instead of just being set.
		The current field must also not be :data:`None`, otherwise nothing
		happens.

		:param dependencies: The list of dependencies. Either a string containing
			a single field, or an iterable containing multiple. This is done
			mostly to preserve the original ``dependencies`` rule's behaviour.
		:param field: The current field.
		:param value: The current field's value.

		The rule's arguments are validated against this schema:
		{
			'type': ['string', 'list']
		}
		"""

		if value is None:
			return

		if isinstance(dependencies, str):
			dependency_list = (dependencies,)
		else:
			dependency_list = dependencies

		for dependency in dependency_list:
			found_name, found_value = self._lookup_field(dependency)

			if found_value is not None:
				continue

			if found_name is None:
				found_name = dependency

			self._error(
				field,
				f"depends on {found_name} being set and not null"
			)

	types_mapping = cerberus.Validator.types_mapping.copy()
	types_mapping["uuid"] = cerberus.TypeDefinition(
		"uuid",
		(uuid.UUID,),
		()
	)


def validate_json(
	schema: typing.Dict[
		str,
		typing.Union[
			str,
			typing.Dict
		]
	],
	*args,
	**kwargs
) -> typing.Callable:
	"""Checks JSON data sent in the :attr:`flask.request` against a Cerberus
	schema. If the validation was sucessful, sets :attr:`flask.g.json` to the
	document the validator outputs - in case there were any coercions or other
	modiciations done by it.

	:param schema: The cerberus schema.
	
	:raises werkzeug.exceptions.BadRequest: Raised when the JSON is invalid.
	"""

	def wrapper(
		func: typing.Callable
	) -> typing.Callable:
		@functools.wraps(func)
		def decorator(*wrapped_args, **wrapped_kwargs) -> typing.Any:
			if flask.request.json is None:
				raise werkzeug.exceptions.BadRequest

			if not isinstance(flask.request.json, dict):
				raise werkzeug.exceptions.BadRequest

			validator = APIValidator(
				schema,
				*args,
				**kwargs
			)

			if not validator.validate(flask.request.json):
				raise werkzeug.exceptions.BadRequest(validator.errors)

			flask.g.json = validator.document

			return func(*wrapped_args, **wrapped_kwargs)
		return decorator
	return wrapper
